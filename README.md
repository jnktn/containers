# jnktn.tv CI containers

This repository contains the dockerfiles which we use to generate custom container images for various stages of our CI. Please do not modify the dockerfiles here. Otherwise, they will get overwritten by the changes in the upstream repository which also builds the images on a weekly schedule.

When our CI system (Woodpecker CI) adds support for the scheduled builds (cron triggers), we should move the building of the images to this repository and host everything here. This functionality is a part of its [development roadmap](https://github.com/woodpecker-ci/woodpecker/issues/869#issue-1191452845).

We also use the official container images (plainly mirrored on quay.io) for a few of our CI jobs.

## latest builds/mirrors

* [quay.io/meisam/trivy](https://quay.io/meisam/trivy) (almalinux + trivy)
* [quay.io/meisam/charliecloud](https://quay.io/meisam/charliecloud) (almalinux + charliecloud, regctl, manifest-tool, cosign)
* [quay.io/meisam/ffmpeg](https://quay.io/meisam/ffmpeg) (ubuntu + ffmpeg)
* [quay.io/meisam/node:alpine](https://quay.io/meisam/node:alpine) (mirror of docker.io/library/node:alpine)
* [quay.io/meisam/caddy:2-alpine](https://quay.io/meisam/caddy:2-alpine) (mirror of docker.io/library/caddy:2-alpine)
