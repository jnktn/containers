#!/bin/sh
set -e
set -x

trivy config --exit-code 1 "$CI_PROJECT_DIR/trivy/Dockerfile"