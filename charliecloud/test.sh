#!/bin/sh
set -e
set -x

ch-image pull "$CI_REGISTRY_IMAGE/charliecloud:${CI_COMMIT_SHORT_SHA}"
ch-image list
ch-image push "$CI_REGISTRY_IMAGE/charliecloud:${CI_COMMIT_SHORT_SHA}" "${CI_REGISTRY_IMAGE}/test" 
ch-convert "$CI_REGISTRY_IMAGE/charliecloud:${CI_COMMIT_SHORT_SHA}" "${CI_PROJECT_DIR}/charliecloud_image"
manifest-tool --username "${CI_REGISTRY_USER}" --password "${CI_REGISTRY_PASSWORD}" inspect "$CI_REGISTRY_IMAGE/charliecloud:${CI_COMMIT_SHORT_SHA}"
regctl registry login "${CI_REGISTRY}" -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}"
regctl image config "$CI_REGISTRY_IMAGE/charliecloud:${CI_COMMIT_SHORT_SHA}"
cosign --help